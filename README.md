VMM: 
DVMM card version 4: Prototype cards and Bonn/Mainz production use
    fecv6_vmm3_DVM4_FLSH64M_vx_x
DVMM card version 4.1 and 5: Mass production cards use 
    fecv6_vmm3_DVM4_1_FLSH64M_vx_x

-------------------------------------------------------------
APV:
--- Fecv3, without Zero Suppression (from wg5 webpage)
--- Fecv6, without Zero Suppression (from Hans)
--- Fecv6, without Zero Suppression with 32 bits Event Counter on the NIM Out (from Michael)
--- Fecv6, without Zero Suppression with 16 bits Event Counter on the NIM Out (from Michael)
--- FecV3, with Zero Suppression  (from Andre’)
--- FecV6, with Zero Suppression  (from Stefano)

--- Fecv3, without Zero Suppression ---
Attached: fec_adc_v211test
Source: https://espace.cern.ch/rd51-wg5/srs/Firmware/FEC/Test/fec_adc_v211test.zip
It is a folder called test and the firmware is commented with the following note: FECv3 raw ADC, version 0.11 - IEEE 802.3x Ethernet flow control enabled
 
--- Fecv6, without Zero Suppression ---
Attached:  fecv6_adc_v1_0_33MH
Source: I got it from Hans via mail exchange, according to your mail this is the default used by the company.
              
--- Fecv6, without Zero Suppression with 32 bits Event Counter on the NIM Out ----
Attached: FEC6_APV_TriggerID_Inverted.bit
Source: I’ve got it from Michael
 
--- Fecv6, without Zero Suppression with 16 bits Event Counter on the NIM Out ---
Attached: FEC6_APV_TriggerID16b_Inverted_64M.mcs
Source: I’ve got it from Michael
 
--- FecV3, with Zero Suppression  ---
Attached: FECv3_APZ_AZ.mcs
Source: I got it from Andre Zibell and I guess that is the original plus minor modification, not sure but I know that works
 
--- FecV6, with Zero Suppression (modified by CMS)  ---
Attached: fecv6_apz
Recompiled by Michael
Source: CMS (Stefano Colafranceschi), from : https://github.com/pakhotin/FECv6_APZ_code
